function hideAllError(str) { return true; }
window.onerror = hideAllError;

$(document).ready(function () {

  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false
  });

  $('.m-header-basket__card-minus').on('click', function (e) {
    e.preventDefault();

    var currentVal = $(this).next().val();

    if (currentVal > 1) {
      $(this).next().val(--currentVal);
    }
  });

  $('.m-header-basket__card-plus').on('click', function (e) {
    e.preventDefault();

    var currentVal = $(this).prev().val();

    if (currentVal < 99) {
      $(this).prev().val(++currentVal);
    }
  });

  $('.m-basket-card__minus').on('click', function (e) {
    e.preventDefault();

    var currentVal = $(this).next().val();

    if (currentVal > 1) {
      $(this).next().val(--currentVal);
    }
  });

  $('.m-basket-card__plus').on('click', function (e) {
    e.preventDefault();

    var currentVal = $(this).prev().val();

    if (currentVal < 99) {
      $(this).prev().val(++currentVal);
    }
  });

  $('.m-form__minus').on('click', function (e) {
    e.preventDefault();

    var currentVal = $(this).next().val();

    if (currentVal > 1) {
      $(this).next().val(--currentVal);
    }
  });

  $('.m-form__plus').on('click', function (e) {
    e.preventDefault();

    var currentVal = $(this).prev().val();

    if (currentVal < 99) {
      $(this).prev().val(++currentVal);
    }
  });

  $('.m-header__basket-button').on('click', function (e) {
    e.preventDefault();

    $('.m-header-basket').slideToggle('fast');
  });

  window.onscroll = function () { scrollFunction() };

  function scrollFunction() {
    if (document.documentElement.scrollTop > 50) {
      $('.m-header').addClass('m-header_fixed');
    } else {
      $('.m-header').removeClass('m-header_fixed');
    }
  }

  new Swiper('.m-welcome', {
    pagination: {
      el: '.m-welcome .swiper-pagination',
      clickable: true
    },
  });

  new Swiper('#combo .m-cards', {
    navigation: {
      nextEl: '#combo .swiper-button-next',
      prevEl: '#combo .swiper-button-prev',
    },
    spaceBetween: 30,
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1024: {
        slidesPerView: 3,
      },
      1200: {
        slidesPerView: 4,
      },
    }
  });

  new Swiper('#roll .m-cards', {
    navigation: {
      nextEl: '#roll .swiper-button-next',
      prevEl: '#roll .swiper-button-prev',
    },
    spaceBetween: 30,
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1024: {
        slidesPerView: 3,
      },
      1200: {
        slidesPerView: 4,
      },
    }
  });

  new Swiper('#salad .m-cards', {
    navigation: {
      nextEl: '#salad .swiper-button-next',
      prevEl: '#salad .swiper-button-prev',
    },
    spaceBetween: 30,
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1024: {
        slidesPerView: 3,
      },
      1200: {
        slidesPerView: 4,
      },
    }
  });

  new Swiper('#drink .m-cards', {
    navigation: {
      nextEl: '#drink .swiper-button-next',
      prevEl: '#drink .swiper-button-prev',
    },
    spaceBetween: 30,
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1024: {
        slidesPerView: 3,
      },
      1200: {
        slidesPerView: 4,
      },
    }
  });

  new Swiper('.m-reviews__cards', {
    navigation: {
      nextEl: '.m-reviews .swiper-button-next',
      prevEl: '.m-reviews .swiper-button-prev',
    },
    pagination: {
      el: '.m-reviews .swiper-pagination',
      clickable: true
    },
    spaceBetween: 30,
    breakpoints: {
      768: {
        slidesPerView: 1,
      },
      1024: {
        slidesPerView: 2,
      },
      1200: {
        slidesPerView: 3,
      },
    }
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();

    $('.m-modal').toggle();
  });

  $('.m-modal__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'm-modal__centered') {
      $('.m-modal').hide();
    }
  });

  $('.m-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.m-modal').hide();
  });

  $(window).on('load resize scroll', function (e) {
    $('body').css({
      'padding-top': $('.m-header').height(),
    })

    if ($(window).width() < 1200) {

      window.onscroll = function () { scrollFunction() };

      function scrollFunction() {
        if (document.documentElement.scrollTop > 50) {
          $('.m-header-mob').addClass('m-header-mob_fixed');
        } else {
          $('.m-header-mob').removeClass('m-header-mob_fixed');
        }
      }
    }
  });

  $(window).on('load resize', function (e) {

    if ($(window).width() < 1200) {

      $('.m-footer__phone').detach().prependTo('.m-footer .container');
      $('.m-footer__button').detach().insertAfter('.m-footer__phone');

      $('.m-footer__link').on('click', function (e) {
        e.preventDefault();

        $(this).next().slideToggle('fast');
        $(this).toggleClass('m-footer__link_active');
      });
    }
  });

  $('.m-header-mob__nav').on('click', function (e) {
    e.preventDefault();

    $('.m-header__top').slideToggle('fast');
    $('.m-header__main').slideToggle('fast');
  });

  $('.m-seo__more').on('click', function (e) {
    e.preventDefault();

    $(this).prev().addClass('m-seo__content_opened');
    $(this).toggle();
  });
});
